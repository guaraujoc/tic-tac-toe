/* Created by Gustavo Carvalho Araújo / NUSP: 13735630
 * University of São Paulo
 * Computer Engineering
 * Introduction to computer science */

#include "values.h"
#include <stdio.h>
#include <stdlib.h>

#define TAMANHO_MATRIZ 3

void cria_matriz(matriz *m) {  // aloca memória

    m->info = malloc(TAMANHO_MATRIZ * sizeof(word *));
    for (int i = 0; i < TAMANHO_MATRIZ; i++) {  // aloca a memória para a matriz multidimensional A[i][j]
        m->info[i] = malloc(TAMANHO_MATRIZ * sizeof(word));
        for(int j = 0; j < TAMANHO_MATRIZ; j++){
            m->info[i][j] = ' ';    // preenche com espaços vazios
        }
    }

}

void deleta_matriz(matriz*m){ //libera memória

    for (int i = 0; i < TAMANHO_MATRIZ; i++) { // libera a memória de cada linha

        free(m->info[i]);

    }

    free(m->info); // libera a memória da matriz

}

void selection(char *a){ // permite o primeiro jogador escolher se quer jogar com X ou O

    printf("Jogador 1, escolha se voce quer jogar com X ou O:\n");
    scanf(" %c", a); // recebe o caracter desejado

    while(*a != 'X' && *a != 'O'){ // se for diferente de X e O, erro
        printf("Caractér inválido, escolha X ou O:\n");
        scanf(" %c", a); // escolha X ou O
    }

    if(*a == 'X'){ // escolheu X

        printf("O jogador 1 sera X e jogador 2 O.\n");

    }
    else{ // escolheu O

        printf("O jogador 1 sera O e jogador 2 X.\n");

    }
}

int insert(matriz* m, char w) { // insere os elementos na matriz

    int i, j;

    printf("Insira a coluna e a linha a jogar: \n");
    scanf("%d %d", &i, &j); // recebe os valores da linha e coluna

    i--; j--; // decrementar i e j para que os índices sejam os esperados pelo usuário

    if (i < 0 || i >= TAMANHO_MATRIZ || j < 0 || j >= TAMANHO_MATRIZ) { // usuário selecionou valores fora da matriz
        printf("Este espaco nao esta disponivel na tabela 3x3!\n");
        return 1; // erro
    }

    if (m->info[i][j] != ' ') { // usário selecionou posição já preenchida
        printf("Esta posicao ja foi selecionada.\n");
        return 1; //erro
    }

    m->info[i][j] = w; // colocou em uma posição válida, preenche a posição da matriz com o caracter

    return 0; // sucesso
}

int change(char a){ // altera o turno

    if(a == 'X'){ // X jogou, vez de O

        return 'O';

    } else { // O jogou, vez de X

        return 'X';

    }

}

// testar se houve ganhador e qual deles ganhou
int testa_ganhador(matriz m, word player1) {
    int i, j, iguais;

    // Testar se uma linha foi completada
    for (i = 0; i < TAMANHO_MATRIZ; i++){ // percorre linha por linha
        iguais = 1; //inicialmente, assume que todos os elementos da linha são iguais
        for(j = 0; j < TAMANHO_MATRIZ - 1; j++){//percorre elementos da linha
            if(m.info[i][j] !=m.info[i][j+1]){ // se encontrar itens sucessores diferentes
                iguais = 0; // indicar que há elementos diferentes
                break; // sair do laço mais cedo
            }
        }
        // se, ao final de um laço não forem encontrados elementos diferentes e
        // os elementos forem não vazios
        if(iguais && m.info[i][0] != ' '){
            printf("Linha %d foi completa!\n", i);
            return m.info[i][0] == player1 ? 1 : 2;
        }
    }

    // Testar se uma coluna foi completada
    for (j = 0; j < TAMANHO_MATRIZ; j++){ // percorre coluna por coluna
        iguais = 1; //inicialmente, assume que todos os elementos da linha são iguais
        for(i = 0; i < TAMANHO_MATRIZ - 1; i++){//percorre elementos da coluna
            if(m.info[i][j] != m.info[i + 1][j]){ // se encontrar itens sucessores diferentes
                iguais = 0; // indicar que há elementos diferentes
                break; // sair do laço mais cedo
            }
        }
        // se, ao final de um laço não forem encontrados elementos diferentes e
        // os elementos forem não vazios
        if(iguais && m.info[0][j] != ' '){
            printf("Coluna %d foi completa!\n", j);
            return m.info[0][j] == player1 ? 1 : 2;
        }
    }

    // Testar diagonal principal
    // Percorrer do canto superior esquero ao canto inferior direito
    iguais = 1;
    for(i = 0; i < TAMANHO_MATRIZ - 1; i++){ //percorrer diagonal principal
        if(m.info[i][i] != m.info[i + 1][i + 1]){
            iguais = 0;
            break;
        }
    }
    if(iguais == 1 && m.info[0][0] != ' '){
        printf("Diagonal principal foi completa!\n");
        return m.info[0][0] == player1 ? 1 : 2;
    }

    // Testar diagonal secundária
    // Percorrer do canto superior direito ao inferior esquerdo
    iguais = 1;
    for(i = 0; i < TAMANHO_MATRIZ - 1; i++){ //percorrer diagonal principal
        if(m.info[i][TAMANHO_MATRIZ - i - 1] != m.info[i + 1][TAMANHO_MATRIZ - i - 2]){
            iguais = 0;
            break;
        }
    }
    if(iguais && m.info[0][TAMANHO_MATRIZ - 1] != ' '){
        printf("Diagonal secundária foi completa!\n");
        return m.info[0][TAMANHO_MATRIZ - 1] == player1 ? 1 : 2;
    }

    return 0; // não houve vitória na rodada

}

void imprime_barra_horizontal(){
    for(int i = 0; i < TAMANHO_MATRIZ; i++){
        printf("-----");
    }
    printf("\n");
}

void imprime_celula(word valor){
    printf("| %c |", valor);
}

void print(matriz m) { // função auxiliar para visualizar matriz
    for(int i = 0; i < TAMANHO_MATRIZ; i++){  // percorre linhas
        imprime_barra_horizontal();

        for(int j = 0; j < TAMANHO_MATRIZ; j++){ // percorre elementos da linha
            imprime_celula(m.info[i][j]);
        }
        printf("\n");
    }

    imprime_barra_horizontal();

}

// Função para criar um jogo novo
// recebe ponteiro de matriz e ponteiro de caracter do jogador 1
void inicia_jogo(matriz *m, word *player){
    cria_matriz(m); // aloca memória da matriz
    selection(player); // usuário seleciona X ou O
}

// Função para tick do jogo
// Recebe ponteiro de matriz e ponteiro de caracter do jogador 1
//
void tick_jogo(matriz *m, word *player){
    printf("De acordo com o tabuleiro, selecione onde ira marcar %c:\n", *player); // mostra de quem é o turno

    while(insert(m, *player) != 0); // verifica se o valor escolhido é valido, caso contrário, pede novamente
    *player = change(*player); // altera o turno

    print(*m); // imprime a matriz com os valores já preenchidos
}

// Laço principal do código
// Recebe ponteiro de matriz e ponteiro de caracter do jogador 1
// Retorna:
// 0 se empate
// 1 se jogador 1 ganhou
// 2 se jogador 2 ganhou
int logica_principal(matriz* m){
    int vencedor, turno;

    word player; // player da rodada atual, muda a cada rodada.

    // nessa linha, é selecionado o player1. Por isso é necessário passagem por referência
    inicia_jogo(m, &player);
    word player1 = player; // player da primeira jogada, utilizado para testar qual jogador venceu

    printf("O tabuleiro inicialmente está vazio:\n");
    print(*m);

    for(int turno = 0; turno < TAMANHO_MATRIZ * TAMANHO_MATRIZ; turno++)
    {
        tick_jogo(m, &player); // nova jogada

        vencedor = testa_ganhador(*m, player1);
        if(vencedor){
            return vencedor;
        }

    }

    // se não houverem vencedores, retornar empate
    return 0;
}
