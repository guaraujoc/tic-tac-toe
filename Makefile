# Created by Gustavo Carvalho Araújo / NUSP: 13735630
# And João Pedro gomes/ NUSP:13839069
CC=gcc
DB=gdb
clean:
	rm *.o

debug: all
	$(DB) ./executavel.o

run: all
	./executavel.o

all: main.o
	$(CC) main.o values.o -o executavel.o

main.o: values.o
	$(CC) -c main.c -o main.o

values.o:
	$(CC) -c values.c -o values.o
