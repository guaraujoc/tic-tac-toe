/* Created by Gustavo Carvalho Araújo / NUSP: 13735630
 * University of São Paulo
 * Computer Engineering
 * Introduction to Computer Science */

typedef char word;

typedef struct{

    word ** info;

} matriz;

void cria_matriz(matriz * m);
void deleta_matriz(matriz*m);
void selection(char *a);
int insert(matriz*m, char w);
int change(char a);
int testa_ganhador(matriz m, word player1);
void print(matriz m);
int logica_principal(matriz* m);
