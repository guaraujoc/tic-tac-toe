## Tic Tac Toe

Author: Gustavo Carvalho Araujo

You can reach me through my LinkedIn profile: [Gustavo Araújo](https://www.linkedin.com/in/guaraujoc/)

## Description

&nbsp;&nbsp;&nbsp;&nbsp;The program is an implementation of the Tic Tac Toe game in C.

## Instructions

&nbsp;&nbsp;&nbsp;&nbsp;The game consists of trying to mark a line, column, or diagonal of a 3 x 3 matrix and preventing your opponent from doing the same. The player who accomplishes this first wins. If all spaces are filled, the game ends in a draw, or as it is popularly known, a "cat's game."

&nbsp;&nbsp;&nbsp;&nbsp;At the start of the game, Player 1 chooses whether to play with 'X' or 'O,' represented by uppercase 'X' and lowercase 'o,' respectively.
&nbsp;&nbsp;&nbsp;&nbsp;Right after Player 1, they should choose the row and column of the matrix to be filled with 'X' or 'O,' respectively.
&nbsp;&nbsp;&nbsp;&nbsp;Opponent's turn.
&nbsp;&nbsp;&nbsp;&nbsp;Steps 2 and 3 repeat until one of the players wins or it's a draw.
Example

```
Player 1, choose whether you want to play with X or O:

  X

  Enter the column and row to play:
  -------------
  |   |   |   |
  -------------
  |   |   |   |
  ----------
  |   |   |   |
  -------------

  1
  1

  -------------
  | X |   |   |
  -------------
  |   |   |   |
  ----------
  |   |   |   |
  -------------

  According to the board, select where to mark O:

  2
  2

  -------------
  | X |   |   |
  -------------
  |   | O |   |
  ----------
  |   |   |   |
  -------------

```


## Notes:

&nbsp;&nbsp;&nbsp;&nbsp;If an invalid space is selected, you will be asked to choose a new space without losing your turn.

&nbsp;&nbsp;&nbsp;&nbsp;If an already filled space is selected, you will be notified, and you can play again in another space without losing your turn.

&nbsp;&nbsp;&nbsp;&nbsp;The game only ends when a player wins or all spaces are filled with no winner, i.e., it's a "cat's game."

&nbsp;&nbsp;&nbsp;&nbsp;Now, enjoy the game, and I hope you have a lot of fun. :)

## Compilation Requirements

&nbsp;&nbsp;&nbsp;&nbsp;C Compiler compatible with C11 or higher standard.

## Project Structure

&nbsp;&nbsp;&nbsp;&nbsp;The project is structured as follows:

&nbsp;&nbsp;&nbsp;&nbsp;The file "main.c" contains the main function of the program.

&nbsp;&nbsp;&nbsp;&nbsp;The file "values.h" is a header file that defines data structures and function prototypes used in the program.

&nbsp;&nbsp;&nbsp;&nbsp;The file "values.c" contains the implementations of the functions defined in the header file "Values.h."

&nbsp;&nbsp;&nbsp;&nbsp;Other source code files can be added to implement additional functionalities.

&nbsp;&nbsp;&nbsp;&nbsp;The code has been implemented using Doubly Linked Lists, which makes it highly compatible with adding new functions and providing functions for use in other codebases.

&nbsp;&nbsp;&nbsp;&nbsp;The "Makefile" contains instructions for compiling the program.

## Contributing

&nbsp;&nbsp;&nbsp;&nbsp;Contributions are welcome! If you have suggestions, improvements, or corrections, please feel free to open an issue or submit a pull request.

## License

&nbsp;&nbsp;&nbsp;&nbsp;This program is licensed under the [MIT License](https://opensource.org/licenses/MIT).
