/* Created by Gustavo Carvalho Araújo / NUSP: 13735630
 * University of São Paulo
 * Computer Engineering
 * Introduction to computer science */

#include <stdio.h>
#include "values.h"

int main(){

    matriz m;

    int vencedor = logica_principal(&m); // Rodará até o fim do jogo

    if(vencedor){
        printf("Jogador %d venceu!\n", vencedor);
    }
    else{
        printf("Deu velha!");
    }

    deleta_matriz(&m);
}
